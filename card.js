var cards = [];
var x,i;
var option = document.getElementById("wrapper");
var namemessage = document.getElementById("namemessage");
var descriptionmessage = document.getElementById("descriptionmessage");
var percentagemessage = document.getElementById("percentagemessage");
var editnamemessage = document.getElementById("editnamemessage");
var editdescriptionmessage = document.getElementById("editdescriptionmessage");
var editpercentagemessage = document.getElementById("editpercentagemessage");
var saveButton = document.getElementById("save");
var updateButton = document.getElementById("update");

/**
 * Open the modal form and display to the user.
 */
function openModalForm(event) {
    if(event.target.id == "addModal") {
        document.getElementById("myModal").classList.toggle("classOption");
    } else if (event.target.id == "editModalClose") {
        document.getElementById("editModal").classList.toggle("classOption");
    }
}

/**
 * Close the modal form.
 */
function closeModalForm(event) {
    var modal;
    if(event.target.id == "createModalClose") {
        modal = document.getElementById("myModal");
        document.getElementById("myModal").classList.toggle("classOption");
    } else if (event.target.id == "editModalClose") {
        modal = document.getElementById("editModal");
        document.getElementById("editModal").classList.toggle("classOption");
    }
}

/**
 * Get the input from the user. 
 */
function createTask() {
    var addTask = {};
    if(inputValidation() == "true"){
        addTask.name = document.getElementById("name").value;
        addTask.description = document.getElementById("description").value;
        addTask.completedPercentage = document.getElementById("percentage").value;
        addTask.status = document.getElementById("status").value;
        createCard(addTask);
    } 
}

/**
 * Get all the tasks.
 * @param {*} data - Array of tasks.
 */
function display(data){
    var dataObject;
    for(i=0; data.length >i ; i++) {
        dataObject = data[i];
        cards.push(dataObject);
        sortTaskByStatus(dataObject.status,dataObject);
    }
}

/**
 * Separate the task based on the status.
 * @param {*} status - Status of the task.
 * @param {*} data - Details of the task.
 */
function sortTaskByStatus(status, data) {
    if(status === "Ready") {
       x = document.getElementById("ready");
       addCard(x, data);
    } else if (status === "Inprogress") {
       x = document.getElementById("inprogress");
       addCard(x, data);
    } else if (status === "Under review") {
       x = document.getElementById("review");
       addCard(x, data);
    } else if (status === "Completed") {
       x = document.getElementById("completed");
       addCard(x, data);
    }
}

/**
 * Create the card division for the new task.
 * @param {*} x - The lane in which the task should be added.
 * @param {*} data - Details of the task.
 */
function addCard(x, data) {
    var card,option,displayname,displaydescription,displaypercentage,displaystatus;
    card = document.createElement("div");
    card.className = "card";
    card.id = "card"+data.id;
    option = createOption(data);
    displayname = createName(data);
    displaydescription = createDescription(data);
    displaypercentage = createPercentage(data);
    card.appendChild(option);
    card.appendChild(displayname);
    card.appendChild(displaydescription);
    card.appendChild(displaypercentage);
    x.appendChild(card);
}

/**
 * Create the divion for edit and delete icons.
 * @param {*} data - Details of the task.
 */
function createOption(data) {
    var option,icon,edit,remove;
    option = document.createElement("div");
    option.className = "option";
    icon = document.createElement("i");
    icon.className = "right fa fa-ellipsis-v";
    icon.id = data.id;
    edit = document.createElement("a");
    edit.id = "edit"+data.id;
    edit.innerText = "Edit";
    edit.className = "edit";
    remove = document.createElement("a");
    remove.id = "delete"+data.id;
    remove.className = "delete";
    remove.innerText = "Delete";
    option.appendChild(icon);
    option.appendChild(edit);
    option.appendChild(remove);
    return option;
}

/**
 * Create the division with name of the task;
 * @param {*} data - Details of the task.
 */
function createName(data) {
    var displayname,label,namespan,namevalue;
    displayname = document.createElement("div");
    displayname.className = "displayname";
    label = document.createElement("label");
    label.innerText = "Name:";
    namespan = document.createElement("span");
    namevalue = document.createElement("p");
    namevalue.innerText = data.name;
    namevalue.id = "namevalue";
    namespan.appendChild(namevalue);
    displayname.appendChild(label);
    displayname.appendChild(namespan);
    return displayname;
}

/**
 * Create the division with description of the task.
 * @param {*} data - Details of the task.
 */
function createDescription(data) {
    var displaydescription,description,descriptionspan,descriptionvalue;
    displaydescription = document.createElement("div");
    displaydescription.className = "displaydescription";
    description = document.createElement("label");
    description.innerText = "Description:";
    descriptionspan = document.createElement("span");
    descriptionvalue = document.createElement("p");
    descriptionvalue.innerText = data.description;
    descriptionvalue.id = "descriptionvalue";
    descriptionspan.appendChild(descriptionvalue);
    displaydescription.appendChild(description);
    displaydescription.appendChild(descriptionspan);
    return displaydescription;
}

/**
 * Create the division with percentage of the task.
 * @param {*} data - Details of the task.
 */
function createPercentage(data) {
    var displaypercentage,percentage,bar;
    displaypercentage = document.createElement("div");
    displaypercentage.className = "displaypercentage";
    bar = document.createElement("a");
    bar.className = "progress-bar";
    bar.id = "percentagevalue";
    bar.innerText = data.completedPercentage + "%";
    bar.style.width = (data.completedPercentage * 2.4) + "px";
    displaypercentage.appendChild(bar);
    return displaypercentage;
}

/**
 * Check the onclick event and forward it to the corresponding function.
 */
option.onclick = function(event) {
    var result;
    if(event.target.className === "right fa fa-ellipsis-v") {
        event.target.nextSibling.classList.toggle("classOption");
        event.target.nextSibling.nextSibling.classList.toggle("classOption");
    } else if(event.target.className.includes("edit")) {
        getTask(event);
    } else if (event.target.className.includes("delete")) {
        deleteCard(event);
        result = findTask(event.target.id.split("delete")[1]);
        cards.splice(cards.indexOf(result),1);
    }
}

/**
 * Get the details of the task by id for update process.
 */
function getTask(event) {
    var result,card;
    result = findTask(event.target.id.split("edit")[1]);
    document.getElementById("hidden").value = result.id;
    document.getElementById("editname").value = result.name;
    document.getElementById("editdescription").value = result.description;
    document.getElementById("editpercentage").value = result.completedPercentage;
    document.getElementById("editstatus").value = result.status;
    document.getElementById("editModal").classList.toggle("classOption");
}

/**
 * Get the details of the task by id.
 * @param {*} id 
 */
function findTask(id) {
    for (i=0; cards.length > i; i++) {
        card = cards[i];
        if(card["id"] == id) {
            result = card;
        }
    }
    return result;
}

/**
 * Get the details of the task to be updated.
 */
function editTask() {
    var editTask = {};
    if(updateInputValidation()) {
        editTask.id = document.getElementById("hidden").value;
        editTask.name = document.getElementById("editname").value;
        editTask.description = document.getElementById("editdescription").value;
        editTask.completedPercentage = document.getElementById("editpercentage").value;
        editTask.status = document.getElementById("editstatus").value;
        editCard(editTask);
    }
}

/**
 * Update the details of the task.
 * @param {*} result - details of the task to be updated.
 */
function updateValue(result) {
    var card;
    document.getElementById("hidden").value = "";
    document.getElementById("editname").value = "";
    document.getElementById("editdescription").value = "";
    document.getElementById("editpercentage").value = "";
    document.getElementById("editModal").style.display = "none";
    card = document.getElementById("card"+result.id);
    card.getElementsByClassName("displayname")[0].querySelector("[id='namevalue']").innerHTML = result.name;
    card.getElementsByClassName("displaydescription")[0].querySelector("[id='descriptionvalue']").innerHTML = result.description;
    card.getElementsByClassName("displaypercentage")[0].querySelector("[id='percentagevalue']").innerHTML = result.completedPercentage;
    cards.splice(cards.indexOf(findTask(result.id)), 1, result);
    location.reload();
}

/**
 * Validate the inputs given by the user.
 */
function inputValidation() {
    var result = "true";
    if (document.getElementById("name").value == "") {
        namemessage.style.display = "block";
        result = "false";
    } else {
        namemessage.style.display = "none";
    } 
    if (document.getElementById("description").value == "") {
        descriptionmessage.style.display = "block";
        result = "false";
    } else {
        document.getElementById("descriptionmessage").style.display = "none";
    } 
    if (document.getElementById("percentage").value == "") {
        percentagemessage.style.display = "block";
        result = "false";
    } else {
        percentagemessage.style.display = "none";
    } 
    return result;
}

/**
 * Disable th save button.
 */
function disableSaveButton(event) {
    event.target.style.border = "2px solid red";
    saveButton.disabled = true;
    saveButton.style.cursor = "not-allowed";
    updateButton.disabled = true;
    updateButton.style.cursor = "not-allowed";
}

/**
 * Enable the save button.
 */
function enableSaveButton(event){
    event.target.style.border = "2px solid green";
    saveButton.disabled = false;
    saveButton.style.cursor = "pointer";
    updateButton.disabled = false;
    updateButton.style.cursor = "pointer";
}

/**
 * Validate the inputs given by the user.
 */
function validateInput(event) {
    if(event.target.id.includes("name")) {
        validateName(event);
    } else if(event.target.id.includes("description")) {
        validateDescription(event);
    } else if(event.target.id.includes("percentage")) {
        validatePercentage(event);
    }
}

/**
 * Validate the name given by the user.
 */
function validateName(event) {
    if(event.target.value == "" || event.target.value.length > 20) {
        disableSaveButton(event);
        namemessage.style.display = "block";
        editnamemessage.style.display = "block";
    } else {
        enableSaveButton(event);
        namemessage.style.display = "none";
        editnamemessage.style.display = "none";
    }
}

/**
 * Validate the description given by the user.
 */
function validateDescription(event) {
    if(event.target.value == "" || event.target.value.length > 160) {
        descriptionmessage.style.display = "block";
        editdescriptionmessage.style.display = "block";
        disableSaveButton(event);
    } else {
        enableSaveButton(event);
        descriptionmessage.style.display = "none";
        editdescriptionmessage.style.display = "none";
    }
}

/**
 * Validate the description given by the user.
 */
function validatePercentage(event) {
    if(event.target.value == "" || event.target.value > 100 || event.target.value < 0) {
        percentagemessage.style.display = "block";
        editpercentagemessage.style.display = "block";
        disableSaveButton(event);
    } else {
        enableSaveButton(event);
        percentagemessage.style.display = "none";
        editpercentagemessage.style.display = "none";
    }
}

/**
 * Validate the inputs given by the user before update process.
 */
function updateInputValidation() {
    var result = "true";
    if (document.getElementById("editname").value == "") {
        editnamemessage.style.display = "block";
        result = "false";
    } else {
        editnamemessage.style.display = "none";
    } 
    if (document.getElementById("editdescription").value == "") {
        editdescriptionmessage.style.display = "block";
        result = "false";
    } else {
        document.getElementById("editdescriptionmessage").style.display = "none";
    } 
    if (document.getElementById("editpercentage").value == "") {
        editpercentagemessage.style.display = "block";
        result = "false";
    } else {
        editpercentagemessage.style.display = "none";
    } 
    return result;
}