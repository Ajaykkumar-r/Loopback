/**
 * Get all the tasks while loadin the page.
 */
$(document).ready(function() { 
    $.ajax({
    type : "GET",
    url : "http://192.168.14.36:4000/api/cards",
    contentType : "application/json",
    dataType : "json",
    success : function(data) {
        display(data);
    }});
});

/**
 * Create the task with details given by the user.
 * @param {*} addTask - Details of the task to be created.
 */
function createCard(addTask) {
    $.ajax({
        type: "POST",
        url: "http://192.168.14.36:4000/api/cards",
        data: addTask,
        success: function(result) {
            document.getElementById("myModal").classList.toggle("classOption");
            document.getElementById("name").value = "";
            document.getElementById("description").value = "";
            document.getElementById("percentage").value = "";
            sortTaskByStatus(result.status, result);
            cards.push(result);
        }
    });
}

/**
 * Update the task with the new details given by the user.
 * @param {*} editTask - Details of the task to be updated.
 */
function editCard(editTask) {
    $.ajax({
        type: "PATCH",
        url: "http://192.168.14.36:4000/api/cards/" + editTask.id,
        data: editTask,
        success: function(result) {
            updateValue(result);
        }
    });
}

/**
 * Delete the task by id. 
 */
function deleteCard(event) {
    $.ajax({
        type: "DELETE",
        url: "http://192.168.14.36:4000/api/cards/" + event.target.id.split("delete")[1],
        success: function(result) {
        event.target.parentElement.parentElement.parentElement.removeChild(document.getElementById("card"+event.target.id.split("delete")[1]));
        }
    });
}